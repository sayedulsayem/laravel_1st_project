<!DOCTYPE html>
<html lang="en">

<head>

    @include('clean-blog.partials.header')

</head>

<body>

    @include('clean-blog.partials.nav')

    @yield('content')

<hr>

    @include('clean-blog.partials.footer')

    @include('clean-blog.partials.scripts')

</body>

</html>
