<?php

Route::get('/', 'PostController@home');

Route::get('/', function (){
    $home="Homepage";
    return view('clean-blog.home',compact('home'));
});

Route::get('/about', function (){
    return view('clean-blog.about');
});
Route::get('/post', function (){
    return view('clean-blog.post');
});
Route::get('/contact', function (){
    return view('clean-blog.contact');
});

